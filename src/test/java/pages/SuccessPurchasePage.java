package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SuccessPurchasePage {

    private static WebDriver driver;
    SuccessPurchasePage(WebDriver driver){
        SuccessPurchasePage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "page-title")
    private WebElement pageTitle;

    @FindBy(className = "sub-title")
    private WebElement pageSubTitle;

    public String getPageTitle(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(pageTitle));
        return pageTitle.getText();
    }

    public String getPageSubTitle(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(pageSubTitle));
        return pageSubTitle.getText();
    }

}