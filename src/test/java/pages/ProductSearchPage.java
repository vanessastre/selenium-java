package pages;

import java.time.Duration;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductSearchPage {

    private static WebDriver driver;
    ProductSearchPage(WebDriver driver){
        ProductSearchPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "product-collection-image-411")
    private WebElement image;

    public ProductPage clickImage(){
        image.click();
        return new ProductPage(driver);
    }

}