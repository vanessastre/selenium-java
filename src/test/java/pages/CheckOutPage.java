package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CheckOutPage {
    private static WebDriver driver;

    CheckOutPage(WebDriver driver){
        CheckOutPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "billing:firstname")
    private WebElement firstName;

    @FindBy(id = "billing:lastname")
    private WebElement lastName;

    @FindBy(id = "billing:company")
    private WebElement company;

    @FindBy(id = "billing:street1")
    private WebElement adress;

    @FindBy(id = "billing:street2")
    private WebElement streeAdress2;

    @FindBy(id = "billing:city")
    private WebElement city;

    @FindBy(id = "billing:region_id")
    private WebElement state;

    @FindBy(id = "billing:postcode")
    private WebElement zipCode;

    @FindBy(id = "billing:country_id")
    private WebElement country;

    @FindBy(id = "billing:telephone")
    private WebElement telephone;

    @FindBy(id = "billing:fax")
    private WebElement fax;

    @FindBy(id = "billing:use_for_shipping_yes")
    private WebElement shipThisAdress;

    @FindBy(id = "allow_gift_messages")
    private WebElement addGiftOptions;

    @FindBy(id = "p_method_cashondelivery")
    private WebElement cashOnDelivery;

    @FindBy(xpath = "//span[contains(text(),'Order')]")
    private WebElement orderButton;

    public void enterFirstName(String firstName){
        this.firstName.sendKeys(firstName);
    }

    public void enterLastName(String lastName){
        this.lastName.sendKeys(lastName);
    }

    public void enterCompany(String company){
        this.company.sendKeys(company);
    }

    public void enterCountry(String country){
        this.country.sendKeys(country);
    }

    public void enterAdress(String adress){
        this.adress.sendKeys(adress);
    }

    public void enterStreeAdress2(String streeAdress2){
        this.streeAdress2.sendKeys(streeAdress2);
    }

    public void enterCity(String city){
        this.city.sendKeys(city);
    }

    public void selectState(String state){
        Select select = new Select(this.state);
        select.selectByVisibleText(state);
    }

    public void enterState(String state){
        this.state.sendKeys(state);
    }

    public void enterZipCode(String zipCode){
        this.zipCode.sendKeys(zipCode);
    }

    public void enterTelephone(String telephone){
        this.telephone.sendKeys(telephone);
    }

    public void enterFax(String fax){
        this.fax.sendKeys(fax);
    }

    public void clickShipThisAdress(){
        this.shipThisAdress.click();
    }

    public void clickCashOnDelivery(){
        this.cashOnDelivery.click();
    }

    public void clickContinueButton(int number){
        WebElement continueButton=driver.findElement(By.xpath("//li["+number+"]//button//span[contains(text(),'Continue')]"));
        continueButton.click();
    }

    public void clickBillingContinueButton() {
        clickContinueButton(1);
    }

    public void clickShippingContinueButton() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(addGiftOptions));
        clickContinueButton(3);
    }

    public void clickPaymenthContinueButton() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(cashOnDelivery));
        clickContinueButton(4);
    }

    public SuccessPurchasePage clickOrderButton(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(orderButton));
        this.orderButton.click();
        return new SuccessPurchasePage(driver);
    }

}