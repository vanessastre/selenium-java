package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage {

    private static WebDriver driver;
    ContactUsPage(WebDriver driver){
        ContactUsPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "name")
    private WebElement name;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "telephone")
    private WebElement telephone;

    @FindBy(id = "comment")
    private WebElement comment;

    @FindBy(xpath = "//span[contains(text(),'Submit')]")
    private WebElement submit;

    public void enterName(String name){
        this.name.sendKeys(name);
    }

    public void enterEmail(String email){
        this.email.sendKeys(email);
    }

    public void enterTelephone(String telephone){
        this.telephone.sendKeys(telephone);
    }

    public void enterComment(String comment){
        this.comment.sendKeys(comment);
    }

}