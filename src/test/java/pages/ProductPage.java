package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductPage {

    private static WebDriver driver;
    ProductPage(WebDriver driver){
        ProductPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//span[contains(text(),'In stock')]")
    private WebElement inStock;

    @FindBy(className = "regular-price")
    private WebElement regularPrice;

    @FindBy(id = "qty")
    private WebElement quantity;

    @FindBy(xpath = "//form[@id='product_addtocart_form']/div[3]/div[6]/div[2]/div[2]/button/span/span")
    private WebElement addToCartButton;

    public void selectColor(String color){
        WebElement selectColor = driver.findElement(By.id("attribute92"));
        Select select = new Select(selectColor);
        select.selectByVisibleText(color);
    }

    public void selectSize(String size){
        WebElement selectSize = driver.findElement(By.id("attribute180"));
        Select select = new Select(selectSize);
        select.selectByVisibleText(size);
    }

    public void enterQuantity(String quantity){
        this.quantity.sendKeys(quantity);
    }

    public CartPage clickAddToCartButton(){
        this.addToCartButton.click();
        return new CartPage(driver);
    }

}