package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
    
    private static WebDriver driver;
    public HomePage(WebDriver driver){
        HomePage.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "logo")
    private WebElement logo;

    @FindBy(id = "select-language")
    private WebElement languageDropdown;

    @FindBy(xpath = "//option[contains(text(),'English')]")
    private WebElement englishLanguage;

    @FindBy(xpath = "//option[contains(text(),'French')]")
    private WebElement frenchLanguage;

    @FindBy(xpath = "//option[contains(text(),'German')]")
    private WebElement germanLanguage;

    @FindAll({
        @FindBy(xpath = "//header//span[contains(text(),'Account')]"),
        @FindBy(xpath = "//header//span[contains(text(),'Compte')]"),
        @FindBy(xpath = "//header//span[contains(text(),'Konto')]")
    })
    private WebElement accountDropdown;

    @FindAll({
        @FindBy(xpath = "//a[contains(text(),'Log')]"),
        @FindBy(xpath = "//a[contains(text(),'Connexion')]"),
        @FindBy(xpath = "//a[contains(text(),'Anmelden')]")
    })
    private WebElement logIn;

    @FindAll({
        @FindBy(xpath = "//a[text()[contains(.,'Register')]]"),
        @FindBy(xpath = "//a[text()[contains(.,'inscrire')]]"),
        @FindBy(xpath = "//a[text()[contains(.,'Registrieren')]]")
    })
    private WebElement register;

    @FindAll({
        @FindBy(xpath = "//header//*//a[contains(text(),'Account')]"),
        @FindBy(xpath = "//header//*//a[contains(text(),'Mon')]"),
        @FindBy(xpath = "//header//*//a[contains(text(),'Mein')]")
    })
    private WebElement myAccount;

    @FindAll({
        @FindBy(xpath = "//span[contains(text(),'Cart')]"),
        @FindBy(xpath = "//span[contains(text(),'Panier')]"),
        @FindBy(xpath = "//span[contains(text(),'Warenkorb')]")
    })
    private WebElement cartButton;

    @FindBy(id = "search")
    private WebElement searchBar;

    @FindAll({
        @FindBy(xpath = "//a[contains(text(),'Advanced')]"),
        @FindBy(xpath = "//a[contains(text(),'Recherche')]"),
        @FindBy(xpath = "//a[contains(text(),'Erweiterte')]")
    })
    private WebElement advancedSearch;

    @FindBy(xpath = "//a[contains(text(),'Contact')]")
    private WebElement contactUs;

    public ProductSearchPage search(String product){
        searchBar.sendKeys(product);
        searchBar.sendKeys(Keys.ENTER);
        return new ProductSearchPage(driver);
    }

    public void selectLanguage(String language){
        WebElement selectLanguage = driver.findElement(By.id("select-language"));
        Select select = new Select(selectLanguage);
        select.selectByVisibleText(language);
    }

    public void clickAccountDropdown(){
       accountDropdown.click();
    }

    public LogInPage clickLogIn(){
        clickAccountDropdown();
        logIn.click();
        return new LogInPage(driver);
    }

    public RegisterPage clickRegister(){
        clickAccountDropdown();
        register.click();
        return new RegisterPage(driver);
    }

    public void clickMyAccount(){
        clickAccountDropdown();
        myAccount.click();
    }

    public void clickCartButton(){
        cartButton.click();
    }

    public AdvancedSearchPage clickAdvancedSearch(){
        advancedSearch.click();
        return new AdvancedSearchPage(driver);
    }

    public ContactUsPage clickContactUs(){
        contactUs.click();
        return new ContactUsPage(driver);
    }

    public HomePage clickLogo(){
        logo.click();
        return new HomePage(driver);
    }

    public String getAccountDropdownText(){
        return accountDropdown.getText();
    }

    public void logOut(){
        clickAccountDropdown();
        driver.findElement(By.linkText("Log Out")).click();
    }

}
