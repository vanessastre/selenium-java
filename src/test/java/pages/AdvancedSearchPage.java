package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearchPage {

    private static WebDriver driver;
    AdvancedSearchPage(WebDriver driver){
        AdvancedSearchPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "name")
    private WebElement name;

    @FindBy(id = "description")
    private WebElement description;

    @FindBy(id = "price")
    private WebElement minPrice;

    @FindBy(id = "price_to")
    private WebElement maxPrice;

    @FindBy(xpath = "//div[2]/button//span[contains(text(),'Search')]")
    private WebElement searchButton;

    public void enterName(String name){
        this.name.sendKeys(name);
    }

    public void enterDescription(String description){
        this.description.sendKeys(description);
    }

    public void enterMinPrice(String minPrice){
        this.minPrice.sendKeys(minPrice);
    }

    public void enterMaxPrice(String maxPrice){
        this.maxPrice.sendKeys(maxPrice);
    }

    public void selectColor(String color){
        WebElement selectColor = driver.findElement(By.id("color"));
        Select select = new Select(selectColor);
        select.selectByVisibleText(color);
    }

    public void selectSize(String size){
        WebElement selectSize = driver.findElement(By.id("size"));
        Select select = new Select(selectSize);
        select.selectByVisibleText(size);
    }

    public void selectGender(String gender){
        WebElement selectGender = driver.findElement(By.id("gender"));
        Select select = new Select(selectGender);
        select.selectByVisibleText(gender);
    }

    public ProductSearchPage clickSearchButton(){
        searchButton.click();
        return new ProductSearchPage(driver);
    }

}