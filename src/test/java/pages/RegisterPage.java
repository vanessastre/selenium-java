package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {

    private static WebDriver driver;
    RegisterPage(WebDriver driver){
        RegisterPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(id = "firstname")
    private WebElement firstName;

    @FindBy(id = "lastname")
    private WebElement lastName;

    @FindBy(id = "email_address")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "confirmation")
    private WebElement confirmPassword;

    @FindBy(id = "is_subscribed")
    private WebElement newsletterCheckBox;

    @FindBy(className = "back-link")
    private WebElement back;

    @FindBy(xpath = "//span[contains(text(),'Register')]")
    private WebElement registerButton;

    public void enterFirstname(String firstName){
        this.firstName.sendKeys(firstName);
    }

    public void enterLastname(String lastName){
        this.lastName.sendKeys(lastName);
    }

    public void enterEmail(String email){
        this.email.sendKeys(email);
    }

    public void enterPassword(String password){
        this.password.sendKeys(password);
    }

    public void enterConfirmPassword(String confirmPassword){
        this.confirmPassword.sendKeys(confirmPassword);
    }

    public void clickNewsletterCheckBox(){
        newsletterCheckBox.click();
    }

    public void clickBack(){
        back.click();
    }

    public HomePage clickRegister(){
        registerButton.click();
        return new HomePage(driver);
    }

}