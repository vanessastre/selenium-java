package pages;

import java.util.ArrayList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ComparePage {

    private static WebDriver driver;
    ComparePage(WebDriver driver){
        ComparePage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//span[contains(text(),'SKU')]")
    private WebElement SKU;

    @FindBy(xpath = "//div[contains(text(),'mtk006c')]")
    private WebElement pulloverSweaterSKU;

    @FindBy(xpath = "//div[contains(text(),'mtk009c')]")
    private WebElement cardiganSweaterSKU;

    @FindBy(xpath = "//span[contains(text(),'Close Window')]")
    private WebElement closeButton;

    public String getSKU(){
        return SKU.getText();
    }

    public String getPulloverSweaterSKU(){
        return pulloverSweaterSKU.getText();
    }

    public String getCardiganSweaterSKU(){
        return cardiganSweaterSKU.getText();
    }

    public void closeCompareWindow(){
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        closeButton.click();
        driver.switchTo().window(tabs.get(0));
    }

}