package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
    private static WebDriver driver;

    CartPage(WebDriver driver){
        CartPage.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(name = "cart[63100][qty]")
    private WebElement quantity;

    @FindBy(xpath = "//div[3]//button//span[contains(text(),'Checkout')]")
    private WebElement checkOutButton;

    @FindBy(xpath = "//td[3]//span[contains(@class, 'price')]//span[1]")
    private WebElement productPrice;

    @FindBy(xpath = "//td[5]//span[contains(@class, 'price')]//span[1]")
    private WebElement subtotalPrice;

    public String getProductPrice(){
        return productPrice.getText();
    }

    public String getSubtotalPrice(){
        return subtotalPrice.getText();
    }

    public void enterQuantity(String quantity){
        this.quantity.sendKeys(quantity);
    }

    public CheckOutPage clickCheckOutButton(){
        checkOutButton.click();
        return new CheckOutPage(driver);
    }

}