package scripts;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import io.qameta.allure.Description;

import pages.HomePage;

public class ChangeLanguageTest extends BaseTest {

    @Test(description="Change language to French")
    @Description("Verify change language to French once the user selects this option")
    public void test() {

        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);

        homePage.selectLanguage("French");

        takeScreenShot("screenshot");
        assertEquals(homePage.getAccountDropdownText(),"COMPTE");
    }

}
