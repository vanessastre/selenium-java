package scripts;

import io.qameta.allure.Description;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.*;

import static org.testng.Assert.assertEquals;

public class AddProductToCart extends BaseTest {
    @Test
    @Description("Verify the addition of a product to the cart")
    public void test() {

        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);

        AdvancedSearchPage advancedSearchPage = homePage.clickAdvancedSearch();

        advancedSearchPage.enterName("sweater");
        advancedSearchPage.selectColor("Red");
        advancedSearchPage.selectSize("S");
        ProductSearchPage productSearchPage = advancedSearchPage.clickSearchButton();

        ProductPage productPage = productSearchPage.clickImage();
        productPage.selectColor("Red");
        productPage.selectSize("S");
        productPage.clickAddToCartButton();

        assertEquals(driver.findElement(By.xpath("//*[contains(@class, 'success-msg')]")).getText(), "Merino V-neck Pullover Sweater was added to your shopping cart.");
        takeScreenShot("screenshot");

    }
}
