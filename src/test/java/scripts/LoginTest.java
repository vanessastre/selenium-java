package scripts;

import dataProviders.AccountData;
import io.qameta.allure.Description;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HomePage;
import pages.LogInPage;
import pages.RegisterPage;

import static org.testng.Assert.assertEquals;

public class LoginTest extends BaseTest{
        @Test
        @Description("Verify the login of the account")
        public void test() {

        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);

        LogInPage logInPage = homePage.clickLogIn();

        logInPage.enterEmail("correo12@gmail.com");
        logInPage.enterPassword("12345678");
        logInPage.clickLoginButton();

        assertEquals(driver.findElement(By.xpath("//p[contains(text(),'Welcome,')]")).getText(), "WELCOME, NOMBRE 1 APELLIDO 1!");
        takeScreenShot("screenshot");

        homePage.logOut();
    }
}
