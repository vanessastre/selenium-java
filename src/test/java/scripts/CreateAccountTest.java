package scripts;

import dataProviders.AccountData;
import io.qameta.allure.Description;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.RegisterPage;

import static org.testng.Assert.assertEquals;

public class CreateAccountTest extends BaseTest{
    @Test(dataProvider = "accounts", dataProviderClass = AccountData.class)
    @Description("Verify the creation of the account")
    public void test(String name, String lastName, String mail, String password, String verifyPassword) throws Exception {

        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);

        RegisterPage registerPage = homePage.clickRegister();

        registerPage.enterFirstname(name);
        registerPage.enterLastname(lastName);
        registerPage.enterEmail(mail);
        registerPage.enterPassword(password);
        registerPage.enterConfirmPassword(verifyPassword);
        registerPage.clickRegister();

        assertEquals(driver.findElement(By.xpath("//html[@id='top']/body/div/div[2]/div[2]/div/div[2]/div/div/ul/li/ul/li/span")).getText(), "Thank you for registering with Madison Island.");
        takeScreenShot("screenshot");

        homePage.logOut();
    }
}