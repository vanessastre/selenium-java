package scripts;

import io.qameta.allure.Allure;

import java.io.ByteArrayInputStream;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class BaseTest {
    public static WebDriver driver;

    private ChromeDriver chromeDriver(){
        System.setProperty("webdriver.chrome.driver","drivers/chromedriver.exe");
        return new ChromeDriver();
    }

    @BeforeClass
    public void setUp() throws Exception {
        driver = chromeDriver();
    }

    public void takeScreenShot(String name) {
		Allure.addAttachment(name, new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
	}
    
    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
