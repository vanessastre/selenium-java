package scripts;

import dataProviders.ProductData;
import io.qameta.allure.Description;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.ProductSearchPage;
import pages.RegisterPage;

import static org.testng.Assert.assertEquals;

public class SearchBarTest extends BaseTest{
    @Test(dataProvider = "products", dataProviderClass = ProductData.class)
    @Description("Verify the search bar functionality")
    public void test(String product) throws Exception {

        driver.get("http://magento-demo.lexiconn.com/");
        HomePage homePage = new HomePage(driver);

        ProductSearchPage productSearchPage = homePage.search(product);
        assertEquals(driver.findElement(By.xpath("//*[contains(@class, 'page-title')]")).getText(), "SEARCH RESULTS FOR '" + product + "'");

        takeScreenShot("screenshot");
    }

}
