package dataProviders;

import org.testng.annotations.DataProvider;

public class ProductData {
    @DataProvider(name = "products")
    public static Object[][] getProductsData() {
        return new Object[][]{
                {"SHOES"},
                {"SHIRT"},
        };
    }
}
