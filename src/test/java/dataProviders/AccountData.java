package dataProviders;

import org.testng.annotations.DataProvider;

public class AccountData {
    @DataProvider(name = "accounts")
    public static Object[][] getAccountData() {
        return new Object[][]{
                {"Nombre 1", "Apellido 1", "correo13@gmail.com", "12345678", "12345678"},
                {"Nombre 2", "Apellido 2", "correo23@gmail.com", "12345678", "12345678"},
        };
    }
}