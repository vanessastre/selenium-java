# Selenium - Java


## Suite:

- Test 1 - Add product to cart
- Test 2 - Change language
- Test 3 - Create account
- Test 4 - Login
- Test 5 - Search bar

To generate the report: ```allure serve allure-results```

## Dependencies

- Java 8+
- Vert.x 3+

## Installation

You can install this project through Maven's Central Repository. In your
`pom.xml`:

```
<dependencies>
  <dependency>
    <groupId>org.example</groupId>
    <artfiactId>pomAbstracta</artifactId>
    <version>1.0-SNAPSHOT</version>
  </dependency>
</dependencies>
```

## References

- Source: ${project.url}
- Documentation: [`/docs`](docs/)
- Issue Tracker: ${project.url}/issues

